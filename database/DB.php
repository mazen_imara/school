<?php

class DB {
    public string $db_host = "localhost";
    public string $username = "user";
    public string $password = "pass";
    public string $database = "school";
    public mysqli $conn;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * @return mysqli
     */
    public function connect()
    {
        $this->conn = new mysqli($this->db_host, $this->username, $this->password);
        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
//        else {
//            print_r('successfully connect');
//        }
    }

    public function select(string $query) : array
    {
//        print_r($query);
        $result = [];

        $ex = $this->conn->query($query);

        while($row = $ex->fetch_assoc()) {
            $result[] = $row;
        }

        $this->close();

        return $result;
    }

    public function alter(string $query) : bool
    {
        print_r($query);
        $result = $this->conn->query($query);
        $this->close();
        return $result;
    }

    public function close()
    {
        $this->conn->close();
    }

    public function createDatabase()
    {
        $exist = false;
        $databases = $this->query('show databases');
        foreach ($databases as $item) {
            if ($item['database'] === $databases) {
                $exist = true;
            }
        }

        if (!$exist) {
            $this->conn->query("CREATE DATABASE {$this->database}");
        }

        $this->close();
    }
}

//
//
//
//
//
//
//
//$q = "show databases";
//$result = $conn->query($q);
//$exist = false;
//if ($result->num_rows > 0) {
//    // output data of each row
//    while($row = $result->fetch_assoc()) {
//        if ($row['Database'] === $database) {
//            $exist = true;
//        }
//    }
//} else {
//    echo "0 results";
//}
//
//if ($exist) {
//    echo "<br> database $database exists.";
//} else {
//    echo "<br> database $database not exists.";
//    // Create database
//    $sql = "CREATE DATABASE $database";
//    if ($conn->query($sql) === TRUE) {
//        echo "<br> Database created successfully";
//    } else {
//        echo "<br> Error creating database: " . $conn->error;
//    }
//}
//
//
//
//
//
//$conn->close();
