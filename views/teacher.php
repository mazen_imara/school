<?php
require_once  __DIR__ . "/../models/Teacher.php";
?>

<html>
    <head>
        <title>School:Teacher</title>
        <?php
        include __DIR__ . '/partials/bootstrap.php';
        ?>
    </head>
    <body>
        <?php
        include __DIR__ . '/partials/header.php';
        ?>
        <h1>Teachers </h1>

        <?php
        include './partials/add-teacher.php';
        ?>

        <?php
        include './partials/all-teacher.php';
        ?>


    </body>
</html>
