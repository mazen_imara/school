<?php
require_once __DIR__ . './../models/Teacher.php';

?>
<html>
<head>
    <title>School:Teacher-Edit</title>
    <?php include __DIR__ . '/partials/bootstrap.php';?>
</head>

<body>
<?php include __dir__ . "/partials/header.php";?>

<h1>Teacher-Editing</h1>
<?php

//print_r($_POST);
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $id = $_GET['id'];
    $teacher = Teacher::get($id);
//    print_r($teacher);
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST;


    $teacher = Teacher::arrayToObject($data);

    $teacher->update();
}

?>
<form action="" method="POST">
    <input type="hidden" value="<?php echo $teacher->id; ?>" name="id">
    <div>
        Personnummer:
        <input type="text" value="<?php echo $teacher->person_number; ?>" name="person_number" required>
    </div>

    <div>
        First name:
        <input type="text" value="<?php echo $teacher->first_name; ?>" name="first_name" required>
    </div>

    <div>
        Last name:
        <input type="text" value="<?php echo $teacher->last_name; ?>" name="last_name" required>
    </div>

    <div>
        Age:
        <input type="number" value="<?php echo $teacher->age; ?>" name="age" required>
    </div>

    <div>
        email:
        <input type="email" value="<?php echo $teacher->email; ?>" name="email" required>
    </div>

    <div>
        Phone:
        <input type="text" value="<?php echo $teacher->phone; ?>" name="phone" required>
    </div>

    <div>
        <input type="submit" value="save" class="btn btn-primary">
    </div>
</form>





    </body>
</html>

