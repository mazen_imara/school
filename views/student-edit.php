<?php
require_once __DIR__ . './../models/Student.php';

?>
<html>
<head>
    <title>School:Student-Edit</title>
    <?php include __DIR__ . '/partials/bootstrap.php';?>
</head>

<body>
    <?php include __dir__ . "/partials/header.php";?>

        <h1>Student-Editing</h1>
    <?php

        //print_r($_POST);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $id = $_GET['id'];
            $student = Student::get($id);
        //    print_r($student);
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = $_POST;

            $student = Student::arrayToObject($data);
            $student->update();


            print_r($student);
        }

    ?>
<form action="" method="POST">
    <input type="hidden" value="<?php echo $student->id; ?>" name="id">
    <div>
        Personnummer:
        <input type="text" value="<?php echo $student->person_number; ?>" name="person_number" required>
    </div>

    <div>
        First name:
        <input type="text" value="<?php echo $student->first_name; ?>" name="first_name" required>
    </div>

    <div>
        Last name:
        <input type="text" value="<?php echo $student->last_name; ?>" name="last_name" required>
    </div>

    <div>
        Age:
        <input type="number" value="<?php echo $student->age; ?>" name="age" required>
    </div>

    <div>
        email:
        <input type="email" value="<?php echo $student->email; ?>" name="email" required>
    </div>

    <div>
        Phone:
        <input type="text" value="<?php echo $student->phone; ?>" name="phone" required>
    </div>

    <div>
        <input type="submit" value="save" class="btn btn-primary">
    </div>
</form>


</body>
</html>

