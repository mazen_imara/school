<?php
    require_once __DIR__ . './../models/Teacher.php';
$id = $_GET['id'];
    $teacher = Teacher::get($id);

?>
<html>
    <head>
        <title>School:Teacher-View</title>
        <?php include __DIR__ . '/partials/bootstrap.php';?>
    </head>

    <body>
        <?php include __dir__ . "/partials/header.php";?>

        <h1>Teacher-View</h1>

        <?php
        include './partials/one-teacher.php';
        ?>

        <div class="teacher-subjects">
            <?php
            $subjects = $teacher->getSubjects();
            $i = 1;
            foreach ($subjects as $subject) {
                echo "{$i}: {$subject->name} <br>";
                $i++;
            }
            ?>
        </div>
    </body>
</html>
