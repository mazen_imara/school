<?php
//require_once  __DIR__ . "/../models/School.php";
?>

<html>
<head>
    <title>School:Home</title>

    <?php
    include __DIR__ . '/partials/bootstrap.php';
    ?>
</head>
<body>
    <?php include __dir__ . "/partials/header.php";?>

    <h1>Welcome to <?php echo School::$name; ?></h1>

    <?php
        include './views/partials/all-students.php';
        include './views/partials/all-teacher.php.php';
    ?>
</body>
</html>
