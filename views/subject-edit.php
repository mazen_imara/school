<?php
require_once __DIR__ . './../models/Subject.php';

?>
    <html>
    <head>
        <title>School:Subject-Edit</title>
        <?php include __DIR__ . '/partials/bootstrap.php';?>
    </head>

    <body>
        <?php include __dir__ . "/partials/header.php";?>

        <h1>Subject-Editing </h1>
        <?php

        //print_r($_POST);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $id = $_GET['id'];
            $subject = Subject::get($id);
            print_r($subject);
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = $_POST;


            $subject = Subject::arrayToObject($data);
            print_r($subject);
            $subject->update();
        }

        ?>

        <form action="" method="POST">
            <input type="text" value="<?php echo $subject->id; ?>" name="id">
            <div>
                name:
                <input type="text" value="<?php echo $subject->name; ?>" name="name" required>
            </div>

            <div>
                number:
                <input type="text" value="<?php echo $subject->number; ?>" name="number" required>
            </div>

            <div>
                description:
                <input type="text" value="<?php echo $subject->description; ?>" name="description" required>
            </div>

            <div>
                <select name="teacher_id">
                    <option value="">----</option>
                    <?php
                    foreach (Teacher::getTeachers() as $teacher) {
                        $isSelected = $teacher->id === $subject->teacher_id ? 'selected' : '';
                        echo "<option {$isSelected} value='{$teacher->id}'>{$teacher->fullName()}</option>";
                    }
                    ;?>
                </select>
            </div>

            <div>
                <input type="submit" value="save" class="btn btn-primary">
            </div>
        </form>


    </body>
    </html>

