<?php
require_once  __DIR__ . "/../models/Subject.php";
?>

<html>
    <head>
        <title>School:Subject</title>
        <?php
        include __DIR__ . '/partials/bootstrap.php';
        ?>
    </head>
    <body>
        <?php include __dir__ . "/partials/header.php";?>

        <h1>Subjects</h1>


        <?php
        include './partials/add-subject.php';
        ?>

        <?php
        include './partials/all-subject.php';
        ?>

    </body>
</html>