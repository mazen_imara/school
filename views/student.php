<?php
require_once  __DIR__ . "/../models/Student.php";
?>

<html>
<head>
    <title>School:Student</title>
    <?php
        include __DIR__ . '/partials/bootstrap.php';
    ?>
</head>
    <body>
        <?php include __dir__ . "/partials/header.php";?>

        <h1>Students</h1>


        <?php
        include './partials/add-student.php';
        ?>

        <?php
        include './partials/all-students.php';
        ?>

    </body>
</html>
