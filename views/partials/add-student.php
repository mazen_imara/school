<?php
require_once  __DIR__ . "/../../models/Student.php";

$data = $_POST;

if ($data['email']) {
//    $student = new Student($data['first_name'], $data['last_name'], $data['email']);
//    $student->person_number = $data['person_number'];
//    $student->age = $data['age'];
//    $student->phone = $data['phone'];
    $student = Student::arrayToObject($data);
    $student->save();
}
?>

<form action="" method="POST">
    <div>
        Personnummer:
        <input type="text" name="person_number" required>
    </div>

    <div>
        First name:
        <input type="text" name="first_name" required>
    </div>

    <div>
        Last name:
        <input type="text" name="last_name" required>
    </div>

    <div>
        Age:
        <input type="number" name="age" required>
    </div>

    <div>
        email:
        <input type="email" name="email" required>
    </div>

    <div>
        Phone:
        <input type="text" name="phone" required>
    </div>

    <div>
        <input type="submit" value="Add student" class="btn btn-primary">
    </div>
</form>
