<?php
require_once __DIR__ . '/../../models/Teacher.php';

    $data = $_POST;

    if ($data['email']) {
        $teacher = new Teacher($data['first_name'], $data['last_name'], $data['email']);
        $teacher->person_number = $data['person_number'];
        $teacher->age = $data['age'];
        $teacher->phone = $data[phone];
        $teacher->save();
    }

    ?>

    <form action="" method="post">

        <div>
            Personnummer:
            <input type="text" name="person_number" required>
        </div>

        <div>
            First name:
            <input type="text" name="first_name" required>
        </div>

        <div>
            Last name:
            <input type="text" name="last_name" required>
        </div>

        <div>
            Age:
            <input type="number" name="age" required>
        </div>

        <div>
            email:
            <input type="email" name="email" required>
        </div>

        <div>
            Phone:
            <input type="text" name="phone" required>
        </div>

        <div>
            <input type="submit" value="Add teacher" class="btn btn-primary">
        </div>

    </form>
