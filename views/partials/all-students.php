<?php
    require_once  __DIR__ . "/../../models/Student.php";

    $students = Student::getStudents();

    foreach ($students as $student) {
        echo "<p>
        {$student->id}:<a href='/views/student-view.php?id={$student->id}'> {$student->fullName()} <br></a>
    </p>";
    }

