<?php
    require_once __DIR__ . '/../../models/Teacher.php';

    $teachers = Teacher::getTeachers();

    foreach ($teachers as $teacher) {
        echo "<p>
        {$teacher->id}:<a href='/views/teacher-view.php?id={$teacher->id}'> {$teacher->fullName()} <br></a>
    </p>";
    }
