<?php
    require_once  __DIR__ . "/../../models/Subject.php";

    $data = $_POST;

    if ($data['name']) {
        $subject = new Subject($data['name'], $data['number'], $data['description']);
        $subject->save();
    }
?>


<form action="" method="post">

    <div>
        subject name:
        <input type="text" name="name" required>
    </div>

    <div>
        subject number:
        <input type="text" name="number" required>
    </div>

    <div>
        subject description:
        <input type="text" name="description" required>
    </div>

    <div>
        <input type="submit" value="Add subject" class="btn btn-primary">
    </div>

</form>

