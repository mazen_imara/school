<?php
require_once __DIR__ . './../models/Student.php';


?>
<html>
    <head>
        <title>School:Student-View</title>
        <?php include __DIR__ . '/partials/bootstrap.php';?>
    </head>

    <body>
        <?php include __dir__ . "/partials/header.php";?>

        <h1>Student-View</h1>

        <?php
        include './partials/one-student.php';
        ?>


    </body>
</html>
