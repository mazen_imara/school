<?php

require_once __DIR__ . './../database/DB.php';
require_once __DIR__ . '/Subject.php';

class Teacher
{
    public static string $table = 'teachers';
    public $id;
    public $first_name;
    public $last_name;
    public $age;
    public $phone;
    public $email;
    public $person_number;

    /**
     * Teacher constructor.
     * @param string $first_name
     * @param string $last_name
     * @param string $email
     * @param int $id
     */
    public function __construct(string $first_name, string $last_name, string $email)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
    }

    public static function getTeachers()
    {
        $teachers = [];
        $db = new DB();
        $t = self::$table;

        $results = $db->select("select * from school.teachers");
            foreach ($results as $result) {
                $teach = new self($result['first_name'], $result['last_name'], $result['email']);
                $teach->id = $result['id'];
                $teach->person_number = $result['person_number'];
                $teach->age =$result['age'];
                $teach->phone = $result['phone'];
                $teachers[] = $teach;
            }

        return $teachers;
    }

    public function fullName() : string
    {
            return "{$this->first_name} {$this->last_name}";
    }

    public function save()
    {
        $db = new DB();
        $t = self::$table;

        $succeeded = $db->alter("insert into {$db->database}.{$t}(first_name, last_name, age, phone, email, person_number) values('{$this->first_name}', '{$this->last_name}', '{$this->age}', '{$this->phone}', '{$this->email}', '{$this->person_number}')");
            if (!$succeeded) {
                print_r('Error not saved');
            }
    }

    public function update()
    {
        $db = new DB();
        $t = self::$table;

        $succeeded = $db->alter("update {$db->database}.{$t} set
            person_number = '{$this->person_number}',
            first_name = '{$this->first_name}',
            last_name = '{$this->last_name}',
            age = '{$this->age}',
            phone = '{$this->phone}',
            email = '{$this->email}'
            where id = {$this->id}
        ");

        if(!$succeeded) {
            print_r('Erorr not updated');
        }

    }

    public static function get($id)
    {
        $db = new DB();
        $t = self::$table;
        $result = $db->select ("SELECT * FROM  {$db->database}.{$t} where id = {$id};");
        if (count($result)) {
            $tea = new self($result[0]['first_name'], $result[0]['last_name'], $result[0]['email']);
            $tea->age= $result[0]['age'];
            $tea->phone= $result[0]['phone'];
            $tea->id= $result[0]['id'];
            $tea->person_number= $result[0]['person_number'];
            return $tea;
        }
    }

    public static function arrayToObject(array $data) : self
    {
        if (isset($data['id'])) {
            $teacher = self::get($data['id']);
        } else {
            $teacher = new self($data['first_name'], $data['last_name'], $data['email']);
        }
        $teacher->first_name = $data['first_name'];
        $teacher->last_name = $data['last_name'];
        $teacher->email = $data['email'];
        $teacher->person_number = $data['person_number'];
        $teacher->age = $data['age'];
        $teacher->phone = $data['phone'];
        return $teacher;
    }

    public function getSubjects()
    {
        $subjects = [];
        $db = new DB();
        $table = Subject::$table;
        $results = $db->select("
            select * from {$db->database}.{$table} where teacher_id = {$this->id}
        ");
        foreach ($results as $result) {
            $subjects[$result['name']] = Subject::arrayToObject($result);
        }
        return  $subjects;
    }
}



