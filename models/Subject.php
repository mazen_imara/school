<?php
require_once __DIR__ . './../database/DB.php';
require_once __DIR__ . '/Teacher.php';

class Subject
{
    public static $table = 'subjects';
    public $id;
    public $name;
    public $number;
    public $description;
    public $teacher_id;

    /**
     * Subject constructor.
     * @param $name
     * @param $number
     * @param $description
     */
    public function __construct($name, $number, $description = '')
    {
        $this->name = $name;
        $this->number = $number;
        $this->description = $description;
    }


    public function save() : void
    {
        $db = new DB();
        $t = self::$table;

        $succeeded = $db->alter("insert into {$db->database}.{$t}(name, number, description) values('{$this->name}', '{$this->number}', '{$this->description}')");
        if (!$succeeded) {
            print_r('Error not saved');
        }
    }

    public function delete() : void
    {
        $db = new DB();
        $t = self::$table;

        $succeeded = $db->alter("DELETE FROM {$db->database}.{$t} where id = {$this->id};");
        if (!$succeeded) {
            print_r('Error not deleted');
        }
    }

    public static function getSubjects() : array
    {
        $subjects = [];
        $db = new DB();
        $t = self::$table;
        $results = $db->select("select * from {$db->database}.{$t}");
        foreach ($results as $result) {
            $subjects[] = self::arrayToObject($result);

        }
        return $subjects;
    }

    public function update() : void
    {
        $db = new DB();
        $t = self::$table;

        $succeeded = $db->alter("update {$db->database}.{$t} set 
            name = '{$this->name}',
            number = '{$this->number}',
            description = '{$this->description}',
            teacher_id = '{$this->teacher_id}'
            where id = {$this->id};
        ");
        if (!$succeeded) {
            print_r('Error not updated');
        }
    }

    public static function get($id)
    {
        $db = new DB();
        $t = self::$table;

        $result = $db->select("SELECT * FROM  {$db->database}.{$t} where id = {$id};");
        if (count($result)) {
            return self::arrayToObject($result[0]);
        }
    }


    public static function arrayToObject(array $data) : self
    {
        $subject = new self($data['name'], $data['number'], $data['description']);
        if (isset($data['id'])) {
            $subject->id = $data['id'];
        }
        $subject->name = $data['name'];
        $subject->number = $data['number'];
        $subject->description = $data['description'];
        $subject->teacher_id = $data['teacher_id'];

        return $subject;
    }

    public function getTeacher()
    {
        return $this->teacher_id ? Teacher::get($this->teacher_id) : null;
    }
}
