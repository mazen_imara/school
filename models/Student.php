<?php

require __DIR__ . './../database/DB.php';

class Student {
    public static string $table = 'students';
    public $id;
    public $first_name;
    public $last_name;
    public  $age;
    public $phone;
    public $email;
    public $person_number;

    /**
     * Student constructor.
     * @param string $first_name
     * @param string $last_name
     * @param int $id
     */
    public function __construct(string $first_name, string $last_name, string $email)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email= $email;
    }

    public function fullName() : string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function vuxenOrUngdom() : string
    {
        return $this->age > 20 ? 'v' : 'u';
    }

    public static function getStudents()
    {
        $students = [];
        $db = new DB();
        $t = self::$table;
        $results = $db->select("select * from school.students");
        foreach ($results as $result) {
            $st = new self($result['first_name'], $result['last_name'], $result['email']);
            $st->id = $result['id'];
            $st->age = $result['age'];
            $st->phone = $result['phone'];
            $students[] = $st;

        }

        return $students;
    }

    public function save() : void
    {
        $db = new DB();
        $t = self::$table;

        $succeeded = $db->alter("insert into {$db->database}.{$t}(first_name, last_name, age, phone, email) values('{$this->first_name}', '{$this->last_name}', '{$this->age}', '{$this->phone}', '{$this->email}')");
        if (!$succeeded) {
            print_r('Error not saved');
        }
    }

    public function update() : void
    {
        $db = new DB();
        $t = self::$table;

        $succeeded = $db->alter("update {$db->database}.{$t} set 
            first_name = '{$this->first_name}',
            last_name = '{$this->last_name}',
            age = '{$this->age}',
            phone = '{$this->phone}',
            email = '{$this->email}',
            person_number = '{$this->person_number}'
            where id = {$this->id};
        ");
        if (!$succeeded) {
            print_r('Error not updated');
        }
    }

    public static function get($id)
    {
        $db = new DB();
        $t = self::$table;

        $result = $db->select ("SELECT * FROM  {$db->database}.{$t} where id = {$id};");
        if (count($result)) {
            $s = new self($result[0]['first_name'], $result[0]['last_name'], $result[0]['email']);
            $s->age= $result[0]['age'];
            $s->phone= $result[0]['phone'];
            $s->id= $result[0]['id'];
            $s->person_number= $result[0]['person_number'];
            return $s;
        }
    }

    public static function arrayToObject(array $data) : self
    {
        if (isset($data['id'])) {
            $student = self::get($data['id']);
        } else {
            $student = new self($data['first_name'], $data['last_name'], $data['email']);
        }
        $student->last_name = $data['last_name'];
        $student->email = $data['email'];
        $student->first_name = $data['first_name'];
        $student->person_number = $data['person_number'];
        $student->age = $data['age'];
        $student->phone = $data['phone'];
        return $student;
    }
}
